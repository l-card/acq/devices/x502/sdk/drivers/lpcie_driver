/***************************************************************************//**
 @file lpcie_ioctls.c
    Файл содержит логику обработки управляющих запросов
 @author Borisov Alexey <borisov@lcard.ru>
 @date  26.04.2021
 ******************************************************************************/
#include <ntddk.h>
#include <wdf.h>
#include "lpcie_trace.h"
#include "lpcie_ioctls.tmh"
#include "lpcie_drv.h"
#include "lpcie_ioctls.h"
#include "lpcie_streams.h"
#include "lpcie_version.h"





/* Тип функции для обработки управляющих запросов */
typedef NTSTATUS (*t_ioctl_func)(IN t_lpcie_dev_context* devExt, IN WDFMEMORY inbuf, IN WDFMEMORY outbuf, size_t *read_cnt);

EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL LpcieEvtIoControl;
static NTSTATUS f_ioctl_fpga_write(IN t_lpcie_dev_context *devExt, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_fpga_read(IN t_lpcie_dev_context *devExt, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_stream_start(IN t_lpcie_dev_context *devExt, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_stream_start_single(IN t_lpcie_dev_context *devExt, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_stream_stop(IN t_lpcie_dev_context *devExt, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_stream_get_rdy(IN t_lpcie_dev_context *devExt, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_stream_set_par(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_stream_get_par(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_stream_free(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_cycle_load(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_cycle_switch(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_cycle_stop(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_cycle_check_setup(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);

static NTSTATUS f_ioctl_power_down(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_reload_devinfo(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);
static NTSTATUS f_ioctl_get_drv_ver(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt);




/* описание обработчика управляющего запроса */
typedef struct {
    ULONG  IoControlCode; /* код запроса */
    int inmem; /* используется ли входной буфер */
    int outmem; /* используется ли выходной буфер */
    t_ioctl_func func; /* функция для обработки запросов */
} t_ioctl_rec;

/* таблица с описаниями обрботчиков запросов */
static t_ioctl_rec f_ioctl_tbl[] = {
    {LPCIE_IOCTL_MEMFPGA_WR,            1, 0, f_ioctl_fpga_write},
    {LPCIE_IOCTL_MEMFPGA_RD,            1, 1, f_ioctl_fpga_read},
    {LPCIE_IOCTL_STREAM_START,          1, 0, f_ioctl_stream_start},
    {LPCIE_IOCTL_STREAM_START_SINGLE,   1, 0, f_ioctl_stream_start_single},
    {LPCIE_IOCTL_STREAM_STOP,           1, 0, f_ioctl_stream_stop},
    {LPCIE_IOCTL_STREAM_SET_PARAMS,     1, 0, f_ioctl_stream_set_par},
    {LPCIE_IOCTL_STREAM_GET_PARAMS,     1, 1, f_ioctl_stream_get_par},
    {LPCIE_IOCTL_STREAM_GET_RDY_SIZE,   1, 1, f_ioctl_stream_get_rdy},
    {LPCIE_IOCTL_STREAM_FREE,           1, 0, f_ioctl_stream_free},
    {LPCIE_IOCTL_CYCLE_LOAD,            1, 0, f_ioctl_cycle_load},
    {LPCIE_IOCTL_CYCLE_SWITCH,          1, 0, f_ioctl_cycle_switch},
    {LPCIE_IOCTL_CYCLE_STOP,            1, 0, f_ioctl_cycle_stop},
    {LPCIE_IOCTL_CYCLE_CHECK_SETUP,     1, 1, f_ioctl_cycle_check_setup},
    {LPCIE_IOCTL_POWER_DONW,            0, 0, f_ioctl_power_down},
    {LPCIE_IOCTL_RELOAD_DEVINFO,        0, 0, f_ioctl_reload_devinfo},
    {LPCIE_IOCTL_GET_DRV_VERSION,       0, 1, f_ioctl_get_drv_ver }
};


static NTSTATUS f_get_word(IN WDFMEMORY buf, uint32_t** wrd) {
    size_t size=0;
    t_status status = STATUS_SUCCESS;
    *wrd = (uint32_t*)WdfMemoryGetBuffer(buf, &size);

    if (size < sizeof(uint32_t)) {
        status = STATUS_INVALID_PARAMETER;
    }
    return status;
}

/* функция вызывается при получении управляющего запроса (когда приложение
   вызывает DeviceIoControl). По таблице f_ioctl_tbl проверяем существование
   запроса, определяем, какие буферы ему нужны, получаем на них указатель и
   вызываем функцию обработки запроса */
VOID LpcieEvtIoControl (
        IN WDFQUEUE  Queue,
        IN WDFREQUEST  Request,
        IN size_t  OutputBufferLength,
        IN size_t  InputBufferLength,
        IN ULONG  IoControlCode
        ) {

    WDFMEMORY inbuf, outbuf;
    NTSTATUS status = STATUS_SUCCESS;
    t_lpcie_dev_context *devExt = LpcieGetDeviceContext(WdfIoQueueGetDevice(Queue));
    ULONG i;
    int fnd = 0;
    size_t readCnt=0;

    UNREFERENCED_PARAMETER(OutputBufferLength);
    UNREFERENCED_PARAMETER(InputBufferLength);
    //PAGED_CODE();


    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_IOCTLS,
                "Receive IOCTL 0x%X", IoControlCode);


    for (i=0; !fnd && (i < sizeof(f_ioctl_tbl)/sizeof(f_ioctl_tbl[0])); i++) {
        if (f_ioctl_tbl[i].IoControlCode == IoControlCode) {
            fnd = 1;
            if (f_ioctl_tbl[i].inmem) {
                status = WdfRequestRetrieveInputMemory(Request, &inbuf);
                if(!NT_SUCCESS(status)) {
                    TraceEvents(TRACE_LEVEL_ERROR, DBG_IOCTLS,
                                "Retrive input buffer for IOCTL 0x%X faild: %!STATUS!",
                                IoControlCode, status);
                }
            }

            if (f_ioctl_tbl[i].outmem && NT_SUCCESS(status)) {
                 status = WdfRequestRetrieveOutputMemory(Request, &outbuf);
                 if(!NT_SUCCESS(status)) {
                     TraceEvents(TRACE_LEVEL_ERROR, DBG_IOCTLS,
                                 "Retrive output buffer for IOCTL 0x%X faild: %!STATUS!",
                                 IoControlCode, status);
                 }
            }

            if (NT_SUCCESS(status)) {
                status = f_ioctl_tbl[i].func(devExt, inbuf, outbuf, &readCnt);
            }
        }
    }

    if (!fnd) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_IOCTLS,
                    "Unsupported IOCTL 0x%X", IoControlCode);
        status  = STATUS_INVALID_DEVICE_REQUEST;
    }

    WdfRequestCompleteWithInformation(Request, status, readCnt);
}



/* Запись значения в регистр ПЛИС. Во входном буфере передается адрес
   и данные в виде струтуры t_lpcie_mem_rw */
static NTSTATUS f_ioctl_fpga_write(IN t_lpcie_dev_context* devExt,
                                   IN WDFMEMORY inbuf, IN WDFMEMORY outbuf, size_t *read_cnt) {
    size_t in_size;
    NTSTATUS status = STATUS_SUCCESS;
    t_lpcie_mem_rw* rw_par;

    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    rw_par = (t_lpcie_mem_rw*)WdfMemoryGetBuffer(inbuf, &in_size);

    if (in_size < sizeof(t_lpcie_mem_rw)) {
        status = STATUS_INVALID_PARAMETER;
    } else {
        status = lpcie_fpga_reg_write(devExt, rw_par->addr, rw_par->val);
    }
    return status;
}

/* Чтение значения регистра ПЛИС.
   Во входном буфере передается адрес, в выходном - прочитанные данные (при успехе) */
static NTSTATUS f_ioctl_fpga_read(IN t_lpcie_dev_context *devExt, IN WDFMEMORY inbuf,
                                  WDFMEMORY outbuf, size_t *read_cnt) {
    size_t in_size, out_size;
    NTSTATUS status = STATUS_SUCCESS;
    ULONG *paddr, *pval;

    paddr = (ULONG*)WdfMemoryGetBuffer(inbuf, &in_size);
    pval = (ULONG*)WdfMemoryGetBuffer(outbuf, &out_size);

    if (in_size < sizeof(ULONG) || (out_size < sizeof(ULONG))) {
        status = STATUS_INVALID_PARAMETER;
    } else {
        status = lpcie_fpga_reg_read(devExt, *paddr, pval);
        if (NT_SUCCESS(status)) {
             *read_cnt = sizeof(ULONG);
        }
    }
    return status;
}


static NTSTATUS f_ioctl_stream_start(IN t_lpcie_dev_context *devExt,
                                        IN WDFMEMORY inbuf, WDFMEMORY outbuf,
                                        size_t *read_cnt) {
    uint32_t *pch;
    t_status status = f_get_word(inbuf, &pch);
    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    if (IS_SUCCESS(status)) {
        status = lpcie_stream_start(devExt, *pch, 0);
    }
    return status;
}

static NTSTATUS f_ioctl_stream_start_single(IN t_lpcie_dev_context *devExt,
                                        IN WDFMEMORY inbuf, WDFMEMORY outbuf,
                                        size_t *read_cnt) {
    uint32_t *pch;
    t_status status = f_get_word(inbuf, &pch);
    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    if (IS_SUCCESS(status)) {
        status = lpcie_stream_start(devExt, *pch, 1);
    }
    return status;
}

static NTSTATUS f_ioctl_stream_stop(IN t_lpcie_dev_context *devExt,
                                        IN WDFMEMORY inbuf, WDFMEMORY outbuf,
                                        size_t *read_cnt) {
    uint32_t *pch;
    t_status status = f_get_word(inbuf, &pch);
    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    if (IS_SUCCESS(status)) {
        status = lpcie_stream_stop(devExt, *pch);
    }
    return status;
}



static NTSTATUS f_ioctl_stream_get_rdy(IN t_lpcie_dev_context *devExt,
                                        IN WDFMEMORY inbuf, WDFMEMORY outbuf,
                                        size_t *read_cnt) {
    uint32_t *pch, *pval;
    t_status status = STATUS_SUCCESS;

    status = f_get_word(inbuf, &pch);
    if (IS_SUCCESS(status)) {
        status = f_get_word(outbuf, &pval);
    }

    if (IS_SUCCESS(status)) {
        status = lpcie_stream_get_rdy_size(devExt, *pch, pval);
    }

    if (IS_SUCCESS(status)) {
        *read_cnt = sizeof(*pval);
    }
    return status;
}



static NTSTATUS f_ioctl_stream_free(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt) {
    uint32_t *pch;
    t_status status = f_get_word(inbuf, &pch);

    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    if (IS_SUCCESS(status)) {
        status = lpcie_stream_free(dev, *pch);
    }
    return status;
}

static NTSTATUS f_ioctl_cycle_load(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf, WDFMEMORY outbuf, size_t *read_cnt) {
    size_t size;
    t_lpcie_cycle_set_par* par;
    t_status status = STATUS_SUCCESS;

    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    par = (t_lpcie_cycle_set_par*)
            WdfMemoryGetBuffer(inbuf, &size);
    if (size < sizeof(t_lpcie_cycle_set_par)) {
        status = STATUS_INVALID_PARAMETER;
    }
    if (IS_SUCCESS(status)) {
        status = lpcie_stream_cycle_load(dev, par);
    }
    return status;
}


static NTSTATUS f_ioctl_cycle_switch(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                     WDFMEMORY outbuf, size_t *read_cnt) {
    size_t size;
    t_lpcie_cycle_evt_par* par;
    t_status status = STATUS_SUCCESS;

    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    par = (t_lpcie_cycle_evt_par*)
            WdfMemoryGetBuffer(inbuf, &size);
    if (size < sizeof(t_lpcie_cycle_evt_par)) {
        status = STATUS_INVALID_PARAMETER;
    }
    if (IS_SUCCESS(status)) {
        status = lpcie_stream_cycle_switch(dev, par);
    }
    return status;
}


static NTSTATUS f_ioctl_cycle_stop(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                   WDFMEMORY outbuf, size_t *read_cnt) {
    size_t size;
    t_lpcie_cycle_evt_par* par;
    t_status status = STATUS_SUCCESS;

    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);

    par = (t_lpcie_cycle_evt_par*)WdfMemoryGetBuffer(inbuf, &size);
    if (size < sizeof(t_lpcie_cycle_evt_par)) {
        status = STATUS_INVALID_PARAMETER;
    }
    if (IS_SUCCESS(status)) {
        status = lpcie_stream_cycle_stop(dev, par);
    }
    return status;
}

static NTSTATUS f_ioctl_cycle_check_setup(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                          WDFMEMORY outbuf, size_t *read_cnt) {
    uint32_t *pch, *pval;
    t_status status = STATUS_SUCCESS;

    status = f_get_word(inbuf, &pch);
    if (IS_SUCCESS(status)) {
        status = f_get_word(outbuf, &pval);
    }

    if (IS_SUCCESS(status)) {
        status = lpcie_stream_cycle_setup_done(dev, *pch, pval);
    }

    if (IS_SUCCESS(status)) {
        *read_cnt = sizeof(*pval);
    }
    return status;
}





static NTSTATUS f_ioctl_stream_set_par(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                       WDFMEMORY outbuf, size_t *read_cnt) {
    size_t size;
    t_lpcie_stream_ch_params* par = (t_lpcie_stream_ch_params*)
            WdfMemoryGetBuffer(inbuf, &size);

    UNREFERENCED_PARAMETER(outbuf);
    UNREFERENCED_PARAMETER(read_cnt);


    return lpcie_streams_set_params(dev, par, size);
}

static NTSTATUS f_ioctl_stream_get_par(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                       WDFMEMORY outbuf, size_t *read_cnt) {
    NTSTATUS status = STATUS_SUCCESS;
    size_t out_size, in_size;
    t_lpcie_stream_ch_params* par = (t_lpcie_stream_ch_params*)
            WdfMemoryGetBuffer(outbuf, &out_size);
    uint32_t *pch = (uint32_t*)WdfMemoryGetBuffer(inbuf, &in_size);
    if (in_size < sizeof(*pch)) {
        status = STATUS_INVALID_PARAMETER;
    } else {
        status = lpcie_streams_get_params(dev, *pch, par, &out_size);
        if (NT_SUCCESS(status))
            *read_cnt = out_size;
    }
    return status;
}




static NTSTATUS f_ioctl_power_down(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                   WDFMEMORY outbuf, size_t *read_cnt) {
    KEVENT event;
    NTSTATUS status;
    PIRP irp;
    IO_STATUS_BLOCK ioStatusBlock;
    PIO_STACK_LOCATION irpStack;
    PDEVICE_OBJECT targetObject;
    PDEVICE_OBJECT DeviceObject;

    //PAGED_CODE();

    DeviceObject = WdfDeviceWdmGetDeviceObject(dev->device);

    KeInitializeEvent( &event, NotificationEvent, FALSE );

    targetObject = IoGetAttachedDeviceReference( DeviceObject );

    irp = IoBuildSynchronousFsdRequest( IRP_MJ_POWER,
                                        targetObject,
                                        NULL,
                                        0,
                                        NULL,
                                        &event,
                                        &ioStatusBlock );

    if (irp == NULL) {
        status = STATUS_INSUFFICIENT_RESOURCES;
        goto End;
    }

    irpStack = IoGetNextIrpStackLocation( irp );

    irpStack->MinorFunction = IRP_MN_SET_POWER;
    irpStack->Parameters.Power.Type = DevicePowerState;
    irpStack->Parameters.Power.State.DeviceState = PowerDeviceD3;
    //irpStack->Parameters.Power.ShutdownType = PowerActionShutdownReset;

    //
    // Initialize the status to error in case the bus driver does not
    // set it correctly.
    //

    irp->IoStatus.Status = STATUS_NOT_SUPPORTED ;

    status = IoCallDriver( targetObject, irp );

    if (status == STATUS_PENDING) {
        KeWaitForSingleObject( &event, Executive, KernelMode, FALSE, NULL );
        status = ioStatusBlock.Status;
    }

End:
    //
    // Done with reference
    //
    ObDereferenceObject( targetObject );

    return status;
}

static NTSTATUS f_ioctl_reload_devinfo(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                       WDFMEMORY outbuf, size_t *read_cnt) {
    return lpcie_read_descr(dev);
}


static NTSTATUS f_ioctl_get_drv_ver(IN  t_lpcie_dev_context *dev, IN WDFMEMORY inbuf,
                                    WDFMEMORY outbuf, size_t *read_cnt) {
    uint32_t* ver;
    t_status status = f_get_word(outbuf, &ver);
    if (IS_SUCCESS(status)) {
        *ver = (LPCIE_DRIVER_VER_MAJOR << 24) |
                (LPCIE_DRIVER_VER_MINOR << 16) |
                (LPCIE_DRIVER_VER_REV   << 8) |
                LPCIE_DRIVER_VER_BUILD;
        *read_cnt = sizeof(*ver);
    }

    return status;
}
