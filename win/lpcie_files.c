/***************************************************************************//**
 @file lpcie_files.c
    Файл содержит callback-функции для работы с файлами: открытие, закрытие,
       чтение, запись.
    Каждое устройство содержит корневой файл (имя которого получено от PnP) -
       для работы с устройством (управляющие запросы, чтение/запись потоков), к
       которому предоставляется эксклюзивный доступ только одного клиента и
       набор дочерних файлов с информацией, доступных только для чтения и
       содержащих информацю о устройстве (имя/серийный и т.д.), к которым могут
       иметь доступ одновременно несколько клиентов.
    При открытии проверяется, что открываемый файл существует и заполняется
       его контекст.
    Запросы чтения/записи к устройству пересылаются в очерди для устройства, а
       к информационным файлам обрабатываются на месте согласно таблице
       f_files_tbl

 @author Borisov Alexey <borisov@lcard.ru>
 @date  26.04.2021
 ******************************************************************************/

#include <ntddk.h>
#include <wdf.h>
#include "lpcie_trace.h"
#include "lpcie_files.tmh"
#include "lpcie_drv.h"
#include "lpcie_streams.h"
#include "l502_fpga_regs.h"


EVT_WDF_DEVICE_FILE_CREATE          LpcieEvtDeviceFileCreate;
EVT_WDF_FILE_CLOSE                  LpcieEvtFileClose;
EVT_WDF_IO_QUEUE_IO_READ            LpcieEvtIoRead;
EVT_WDF_IO_QUEUE_IO_WRITE           LpcieEvtIoWrite;

static NTSTATUS f_devname_read(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done);
static NTSTATUS f_serial_read(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done);
static NTSTATUS f_file_read_opened(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done);
static NTSTATUS f_file_read_bf(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done);
static NTSTATUS f_file_read_dac(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done);
static NTSTATUS f_file_read_gal(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done);

#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, LpcieEvtDeviceFileCreate)
#pragma alloc_text (PAGE, LpcieEvtFileClose)
#endif

/* таблица с именами информационных файлов и функциями чтения из них */
static t_lpcie_file_descr f_files_tbl[] = {
    {L"\\name", LPCIE_FILETYPE_NAME, f_devname_read},
    {L"\\sn", LPCIE_FILETYPE_SN, f_serial_read},
    {L"\\opened", LPCIE_FILETYPE_OPENED, f_file_read_opened},
    {L"\\bf", LPCIE_FILETYPE_BF_PRESENT, f_file_read_bf},
    {L"\\dac", LPCIE_FILEYTPE_DAC_PRESENT, f_file_read_dac},
    {L"\\gal", LPCIE_FILETYPE_GAL_PRESENT, f_file_read_gal}
};


/* Функция вызывается при попытке открыть файл.
   Для открытия файла устройства обеспечиваем эксклюзивность доступа.
   Для информационных файлов - проверяем, что файл существует (по таблице
      f_files_tbl) и заполняем контекст файла полями из таблицы */
VOID LpcieEvtDeviceFileCreate(IN WDFDEVICE  Device,
                              IN WDFREQUEST  Request,
                              IN WDFFILEOBJECT  FileObject) {
    NTSTATUS status = STATUS_SUCCESS;
    UNICODE_STRING   testStr;
    PUNICODE_STRING   fileName;
    LPCIE_FILE_CONTEXT *context;
    t_lpcie_dev_context *devExt ;
    PAGED_CODE();

    devExt = LpcieGetDeviceContext(Device);
    fileName = WdfFileObjectGetFileName(FileObject);
    context = LpcieGetFileContext(FileObject);

    TraceEvents(TRACE_LEVEL_VERBOSE, DBG_FILES, "Try to open file %wZ", fileName);

    /* если имя файла пустое - попытка открыть само устройство */
    if (!fileName->Length) {
        ULONG is_opened = InterlockedCompareExchangeAcquire(&devExt->is_opened, 1,0);
        if (!is_opened) {
            context->descr.ftype = LPCIE_FILETYPE_DEVICE;
            context->device = devExt;
        } else {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_FILES, "Try to open already opened device");
            status = STATUS_ACCESS_DENIED;
        }
    } else {
        /* иначе - идет обращение к информационному файлу - ищем имя файла
           из таблицы f_files_tbl */
        int fnd, i;
        for (fnd= 0, i=0; !fnd && (i<sizeof(f_files_tbl)/sizeof(f_files_tbl[0])); i++) {
            RtlInitUnicodeString(&testStr, f_files_tbl[i].name);

            if (!RtlCompareUnicodeString(&testStr, fileName, FALSE)) {
                fnd = 1;
                context->descr = f_files_tbl[i];
                context->device = devExt;
            }
        }

        if (!fnd) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_FILES, "try to open invalid file: %wZ",
                        fileName);
            status = STATUS_NO_SUCH_FILE;
        }
    }
    WdfRequestComplete(Request, status);
}


/* Функция вызывается при закрытии файла.
   Нас интересует только закрытие файла самого устройство - по этому
   событию мы останавливаем DMA, если оно было запущено, и
   даем доступ для нового открытия устройтсва */
VOID LpcieEvtFileClose(IN WDFFILEOBJECT  FileObject) {
    PUNICODE_STRING fileName;
    LPCIE_FILE_CONTEXT *context;
    PAGED_CODE();

    fileName = WdfFileObjectGetFileName(FileObject);
    context = LpcieGetFileContext(FileObject);

    TraceEvents(TRACE_LEVEL_VERBOSE, DBG_FILES, "Close file %wZ", fileName);

    if (!fileName->Length) {
        LONG is_opened;
        lpcie_streams_dev_release(context->device);
        is_opened = InterlockedCompareExchangeRelease(&context->device->is_opened, 0,1);
        if (!is_opened) {
            TraceEvents(TRACE_LEVEL_WARNING, DBG_FILES, "Try close closed device!");
        }
    }
}

/* Функция вызывается при запросе на чтение из файла.
   Чтение из информационных файлов обслуживаем сразу (параллельно), а чтение
   из устройства передаем в последовательную очередь readDevQueue */
VOID LpcieEvtIoRead (IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t Length) {
    LPCIE_FILE_CONTEXT *context;
    t_lpcie_dev_context *dev = LpcieGetDeviceContext(WdfIoQueueGetDevice(Queue));

    context = LpcieGetFileContext(WdfRequestGetFileObject(Request));


    if (context->descr.ftype==LPCIE_FILETYPE_DEVICE) {
        /* Запросы для устройства передаем в отдельную очередь */
        NTSTATUS status = WdfRequestForwardToIoQueue(Request, dev->readDevQueue);
        if (!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_FILES,
                        "Read request requeue error: %!STATUS!", status);
            WdfRequestComplete(Request, status);
        }
    } else {
        /* информационные запросы обрабатываем тут же */
        ULONG rd_done = 0;
        NTSTATUS status;

        PMDL mdl;
        PVOID addr;

        status = WdfRequestRetrieveOutputWdmMdl(Request, &mdl);
        if (!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_FILES,
                        "Retrive read MDL buffer failed: %!STATUS!", status);
        } else {
            addr = MmGetMdlVirtualAddress(mdl);
            if (context->descr.read) {
                status = context->descr.read(dev, addr, Length, &rd_done);
            } else {
                TraceEvents(TRACE_LEVEL_ERROR, DBG_FILES,
                            "Read request is not supported for file!");
                status = STATUS_INVALID_DEVICE_REQUEST;
            }
        }

        WdfRequestCompleteWithInformation(Request, status, rd_done);
    }
}

/* Функция записи. Запись в информационные файлы не поддерживается,
   а запись в устройство передается в последовательную очередь writeDevQueue */
VOID LpcieEvtIoWrite (IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t Length) {
    LPCIE_FILE_CONTEXT *context;

    context = LpcieGetFileContext(WdfRequestGetFileObject(Request));

    if (context->descr.ftype==LPCIE_FILETYPE_DEVICE) {
        t_lpcie_dev_context *dev = LpcieGetDeviceContext(WdfIoQueueGetDevice(Queue));
        /* Запросы для устройства передаем в отдельную очередь */
        NTSTATUS status = WdfRequestForwardToIoQueue(Request, dev->writeDevQueue);
        if (!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_FILES,
                        "Read request requeue error: %!STATUS!", status);
            WdfRequestComplete(Request, status);
        }
    } else {
        NTSTATUS status = STATUS_INVALID_DEVICE_REQUEST;
        TraceEvents(TRACE_LEVEL_ERROR, DBG_FILES,
                    "Write request is not supported for file!");
        WdfRequestCompleteWithInformation(Request, status, 0);
    }
}




/* Функция вызывается при отмене запроса на чтение данных из устройтсва */
VOID LpcieEvtReqCancel (IN WDFREQUEST  Request) {
    t_lpcie_req_context *context = LpcieGetReqContext(Request);

    WdfWaitLockAcquire(context->state->work_lock, NULL);
    /* устанавливаем признак, что запрос больше не действителен */
    context->state->req_valid = 0;
    /* завершаем со статусом - отменен, указывая кол-во данных, которые были
      обработаны */
    WdfRequestCompleteWithInformation(Request, STATUS_CANCELLED, context->proc_size*4);


    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_DMA, "!!! ------- Request cancelled (ch=%d) with size %d",
                context->state->ch, context->proc_size);

    WdfWaitLockRelease(context->state->work_lock);

}


static VOID f_proc_req(IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t Length,
                       uint32_t ch) {
    t_lpcie_req_context *req = LpcieGetReqContext(Request);
    t_lpcie_dev_context *dev = LpcieGetDeviceContext(WdfIoQueueGetDevice(Queue));
    NTSTATUS status=STATUS_SUCCESS;
    t_lpcie_dma_state* state = &dev->dma[ch];

    TraceEvents(TRACE_LEVEL_VERBOSE, DBG_TRANSF,
                "!!! ------  Start request (ch=%d). Length = %d", ch, Length);

    /* чтение всегда должно быть кратно слову! */
    if (Length%4) {
        status = STATUS_INVALID_DEVICE_REQUEST;
        TraceEvents(TRACE_LEVEL_ERROR, DBG_TRANSF,
                    "Invalid read size: %d", Length);
    } else {
        WDFMEMORY buffer;
        /* получаем указатель на буфер для передачи или приема данных данных */
        if (state->dir == LPCIE_DMA_DIR_IN) {
            status = WdfRequestRetrieveOutputMemory(Request, &buffer);
        } else {
            status = WdfRequestRetrieveInputMemory(Request, &buffer);
        }

        if (!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_TRANSF,
                        "Retrieve request (for ch = %d) buffer error: %!STATUS!",
                        ch, status);
        } else {
            //context->addr = WdfMemoryGetBuffer(buffer, NULL);
            req->buf = buffer;
        }
    }

    /* заполняем параметры текущего запроса и пытаемся его обслужить */
    if (NT_SUCCESS(status)) {
        req->proc_size = 0;
        req->req_size = Length/4;
        req->dev = dev;
        req->state = &dev->dma[L502_DMA_CHNUM_IN];

        //WdfWaitLockAcquire( dev->dma[L502_DMA_CHNUM_IN].work_lock, NULL);

        state->req = Request;
        state->req_valid = 1;

        /* помечаем запрос, что он может быть отменен */
        status = WdfRequestMarkCancelableEx(Request, LpcieEvtReqCancel);

        //WdfWaitLockRelease(dev->dma[L502_DMA_CHNUM_IN].work_lock);

        if (status==STATUS_CANCELLED) {
            /* если он уже отменен - то выполняем отмену прям тут */
            LpcieEvtReqCancel(Request);
        } else if (!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_TRANSF,
                        "WdfRequestMarkCancelableEx error: %!STATUS!", status);
            WdfRequestComplete(Request, status);
        } else {
            /* иначе - пытаемся обработать запрос */
            lpcie_streams_proc_transf(dev, state->ch);
        }
    } else {
        /* при ошибке сразу завершаем запрос */
        WdfRequestComplete(Request, status);
    }
}



VOID LpcieEvtDeviceRead (IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t Length) {
    f_proc_req(Queue, Request, Length, L502_DMA_CHNUM_IN);
}

VOID LpcieEvtDeviceWrite (IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t Length) {
    f_proc_req(Queue, Request, Length, L502_DMA_CHNUM_OUT);
}

static NTSTATUS f_devname_read(t_lpcie_dev_context* devExt, PVOID buf,
                              size_t len, ULONG* rd_done) {
    if (len > sizeof(devExt->name))
        len = sizeof(devExt->name);

    memcpy(buf, devExt->name, len);
    *rd_done = len;
    return STATUS_SUCCESS;
}

static NTSTATUS f_serial_read(t_lpcie_dev_context* devExt, PVOID buf,
                              size_t len, ULONG* rd_done) {
    if (len > sizeof(devExt->sn))
        len = sizeof(devExt->sn);

    memcpy(buf, devExt->sn, len);
    *rd_done = len;
    return STATUS_SUCCESS;
}

static NTSTATUS f_file_read_opened(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done) {
    if (len >= 1) {
        char val = devExt->is_opened ? '1' : '0';
        memcpy(buf, &val, 1);
        *rd_done = 1;
    }
    return STATUS_SUCCESS;
}

static NTSTATUS f_file_read_bf(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done) {
    if (len >= 1) {
        char val = devExt->devflags & LPCIE_DEV_FLAGS_BF ? '1' : '0';
        memcpy(buf,&val , 1);
        *rd_done = 1;
    }
    return STATUS_SUCCESS;
}

static NTSTATUS f_file_read_dac(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done) {
    if (len >= 1) {
        char val = devExt->devflags & LPCIE_DEV_FLAGS_DAC ? '2' : '0';
        memcpy(buf, &val ,1);
        *rd_done = 1;
    }
    return STATUS_SUCCESS;
}

static NTSTATUS f_file_read_gal(t_lpcie_dev_context *devExt, PVOID buf,
                              size_t len, ULONG* rd_done) {
    if (len >= 1) {
        char val = devExt->devflags & LPCIE_DEV_FLAGS_GAL ? '1' : '0';
        memcpy(buf, &val, 1);
        *rd_done = 1;
    }
    return STATUS_SUCCESS;
}
