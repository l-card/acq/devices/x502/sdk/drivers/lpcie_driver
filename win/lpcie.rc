#include <windows.h>

#define OFFICIAL_BUILD 1


#define STRINGER(w, x, y, z) #w "." #x "." #y "." #z

#define XSTRINGER(w, x, y, z) STRINGER(w, x, y, z)

#include "lpcie_version.h"

#define VER_PRODUCTVERSION LPCIE_DRIVER_VER_MAJOR,LPCIE_DRIVER_VER_MINOR,LPCIE_DRIVER_VER_REV,LPCIE_DRIVER_VER_BUILD
#define VER_PRODUCTVERSION_STR XSTRINGER(LPCIE_DRIVER_VER_MAJOR,LPCIE_DRIVER_VER_MINOR,LPCIE_DRIVER_VER_REV,LPCIE_DRIVER_VER_BUILD)


#define VER_COMPANYNAME_STR         "L-Card Ltd"
#define VER_PRODUCTNAME_STR         "KMDF Driver for L-Card PCI Express boards"
#define VER_FILEDESCRIPTION_STR     "KMDF Driver for L-Card PCI Express boards"
#define VER_INTERNALNAME_STR        "lpcie.sys"
#define VER_LEGALCOPYRIGHT_STR      "� 2015 L-Card Ltd."


/* default is nodebug */
#if DBG
#define VER_DEBUG                   VS_FF_DEBUG
#else
#define VER_DEBUG                   0
#endif

/* default is prerelease */
#if BETA
#define VER_PRERELEASE              VS_FF_PRERELEASE
#else
#define VER_PRERELEASE              0
#endif

#if OFFICIAL_BUILD
#define VER_PRIVATE                 0
#else
#define VER_PRIVATE                 VS_FF_PRIVATEBUILD
#endif

#define VER_FILEFLAGSMASK           VS_FFI_FILEFLAGSMASK
#define VER_FILEOS                  VOS_NT_WINDOWS32
#define VER_FILEFLAGS               (VER_PRERELEASE|VER_DEBUG|VER_PRIVATE)





#define VER_FILETYPE                VFT_DRV
#define VER_FILESUBTYPE             VFT2_DRV_SYSTEM




#ifndef VER_FILEVERSION
#define VER_FILEVERSION VER_PRODUCTVERSION
#endif

#ifndef VER_FILEVERSION_STR
    #define VER_FILEVERSION_STR VER_PRODUCTVERSION_STR
#endif

#ifndef VER_ORIGINALFILENAME_STR
    #define VER_ORIGINALFILENAME_STR VER_INTERNALNAME_STR
#endif


#ifdef VER_LANGNEUTRAL
 #ifndef VER_VERSION_UNICODE_LANG
  #define VER_VERSION_UNICODE_LANG  "000004B0" /* LANG_NEUTRAL/SUBLANG_NEUTRAL, Unicode CP */
 #endif
 #ifndef VER_VERSION_ANSI_LANG
  #define VER_VERSION_ANSI_LANG     "000004E4" /* LANG_NEUTRAL/SUBLANG_NEUTRAL, Ansi CP */
 #endif
 #ifndef VER_VERSION_TRANSLATION
  #define VER_VERSION_TRANSLATION   0x0000, 0x04B0
 #endif
#else
 #ifndef VER_VERSION_UNICODE_LANG
  #define VER_VERSION_UNICODE_LANG  "040904B0" /* LANG_ENGLISH/SUBLANG_ENGLISH_US, Unicode CP */
 #endif
 #ifndef VER_VERSION_ANSI_LANG
  #define VER_VERSION_ANSI_LANG     "0c0904E4" /* LANG_ENGLISH/SUBLANG_ENGLISH_US, Ansi CP */
 #endif
 #ifndef VER_VERSION_TRANSLATION
  #define VER_VERSION_TRANSLATION   0x0409, 0x04B0
 #endif
#endif


VS_VERSION_INFO VERSIONINFO
FILEVERSION    VER_FILEVERSION
PRODUCTVERSION VER_PRODUCTVERSION
FILEFLAGSMASK  VER_FILEFLAGSMASK
FILEFLAGS      VER_FILEFLAGS
FILEOS         VER_FILEOS
FILETYPE       VER_FILETYPE
FILESUBTYPE    VER_FILESUBTYPE
BEGIN
    BLOCK "StringFileInfo"
    BEGIN
        BLOCK VER_VERSION_UNICODE_LANG
        BEGIN
            VALUE "CompanyName",     VER_COMPANYNAME_STR
            VALUE "FileDescription", VER_FILEDESCRIPTION_STR
            VALUE "FileVersion",     VER_FILEVERSION_STR
            VALUE "InternalName",    VER_INTERNALNAME_STR
            VALUE "LegalCopyright",  VER_LEGALCOPYRIGHT_STR
            VALUE "OriginalFilename",VER_ORIGINALFILENAME_STR
            VALUE "ProductName",     VER_PRODUCTNAME_STR
            VALUE "ProductVersion",  VER_PRODUCTVERSION_STR
        END

#ifdef VER_ANSICP   /* Some apps are hard coded to look for ANSI CP. */
    BLOCK VER_VERSION_ANSI_LANG
        BEGIN
            VALUE "CompanyName",     VER_COMPANYNAME_STR
            VALUE "FileDescription", VER_FILEDESCRIPTION_STR
            VALUE "FileVersion",     VER_FILEVERSION_STR
            VALUE "InternalName",    VER_INTERNALNAME_STR
            VALUE "LegalCopyright",  VER_LEGALCOPYRIGHT_STR
            VALUE "OriginalFilename",VER_ORIGINALFILENAME_STR
            VALUE "ProductName",     VER_PRODUCTNAME_STR
            VALUE "ProductVersion",  VER_PRODUCTVERSION_STR
        END
#endif
    END

    BLOCK "VarFileInfo"
    BEGIN
        VALUE "Translation", VER_VERSION_TRANSLATION
    END
END


