#include <ntddk.h>
#include <wdf.h>
#include <initguid.h> // required for GUID definitions
#include <wdmguid.h>  // required for WMILIB_CONTEXT
#include "lpcie_trace.h"
#include "lpcie.tmh"
#include "lpcie_drv.h"
#include "lpcie_streams.h"
#include "l502_fpga_regs.h"



DRIVER_INITIALIZE                   DriverEntry;
EVT_WDF_DRIVER_DEVICE_ADD           LpcieEvtDeviceAdd;
EVT_WDF_DRIVER_UNLOAD               LpcieEvtDriverUnload;
EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL  LpcieEvtIoControl;
EVT_WDF_DEVICE_PREPARE_HARDWARE     LpcieEvtPrepareHardware;
EVT_WDF_DEVICE_RELEASE_HARDWARE     LpcieEvtReleaseHardware;
EVT_WDF_DEVICE_D0_EXIT              LpcieEvtD0Exit;
EVT_WDF_DEVICE_FILE_CREATE          LpcieEvtDeviceFileCreate;
EVT_WDF_FILE_CLOSE                  LpcieEvtFileClose;
EVT_WDF_IO_QUEUE_IO_READ            LpcieEvtIoRead;
EVT_WDF_IO_QUEUE_IO_WRITE           LpcieEvtIoWrite;
EVT_WDF_IO_QUEUE_IO_READ            LpcieEvtDeviceRead;
EVT_WDF_IO_QUEUE_IO_WRITE           LpcieEvtDeviceWrite;
EVT_WDF_REQUEST_CANCEL              LpcieEvtReqCancel;


EVT_WDF_INTERRUPT_ISR               LpcieEvtInterruptIsr;
EVT_WDF_INTERRUPT_DPC               LpcieEvtInterruptDpc;
EVT_WDF_INTERRUPT_ENABLE            LpcieEvtInterruptEnable;
EVT_WDF_INTERRUPT_DISABLE           LpcieEvtInterruptDisable;



static NTSTATUS LpcieReadConfigSpace(
    IN PDEVICE_OBJECT DeviceObject,
    IN PVOID          Buffer,
    IN ULONG          Offset,
    IN ULONG          Length
    );


#ifdef ALLOC_PRAGMA
#pragma alloc_text (INIT, DriverEntry)
#pragma alloc_text (PAGE, LpcieEvtDeviceAdd)
#pragma alloc_text (PAGE, LpcieEvtDriverUnload)
#pragma alloc_text (PAGE, LpcieEvtPrepareHardware)
#pragma alloc_text (PAGE, LpcieEvtReleaseHardware)
#pragma alloc_text (PAGE, LpcieReadConfigSpace)
#endif




// {53869b9a-7875-4fd3-9e04-bec81a92f9a9}
DEFINE_GUID (GUID_LPCIE_INTERFACE,
   0x53869b9a, 0x7875, 0x4fd3, 0x9e, 0x04, 0xbe, 0xc8, 0x1a, 0x92, 0xf9, 0xa9);



NTSTATUS DriverEntry(IN PDRIVER_OBJECT  DriverObject,
                     IN PUNICODE_STRING RegistryPath) {
    NTSTATUS            status = STATUS_SUCCESS;
    WDF_DRIVER_CONFIG   config;

    /* Initialize WDF WPP tracing. */
    WPP_INIT_TRACING( DriverObject, RegistryPath );
    /* Include this macro to support Windows 2000 */
    WPP_SYSTEMCONTROL(DriverObject);

    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_INIT,
                "Lpcie (Driver for L Card PCI Express devices) loaded.");
    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_INIT,
                "Built %s %s", __DATE__, __TIME__);


    WDF_DRIVER_CONFIG_INIT(&config, LpcieEvtDeviceAdd);
    config.EvtDriverUnload = LpcieEvtDriverUnload;


    /* Создаем объект драйвера */
    status = WdfDriverCreate(
        DriverObject,
        RegistryPath,
        WDF_NO_OBJECT_ATTRIBUTES, // Driver Attributes
        &config,          // Driver Config Info
        WDF_NO_HANDLE
        );

    if (!NT_SUCCESS(status)) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                    "WdfDriverCreate failed with status %!STATUS!", status);
        //
        // Cleanup tracing here because DriverContextCleanup will not be called
        // as we have failed to create WDFDRIVER object itself.
        // Please note that if your return failure from DriverEntry after the
        // WDFDRIVER object is created successfully, you don't have to
        // call WPP cleanup because in those cases DriverContextCleanup
        // will be executed when the framework deletes the DriverObject.
        //
        WPP_CLEANUP(DriverObject);
    }

    return status;
}

/* Функция вызывается при выгрузки драйвера из памяти */
VOID LpcieEvtDriverUnload(IN WDFDRIVER Driver) {
    PAGED_CODE ();
    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_INIT, "lpcie driver unloaded");
    WPP_CLEANUP( WdfDriverWdmGetDriverObject( Driver ) );
}



/* Функцию вызывает PnP менеджер при обнаружении нового устройства для драйвера */
NTSTATUS LpcieEvtDeviceAdd(IN WDFDRIVER Driver, IN PWDFDEVICE_INIT DeviceInit) {
    NTSTATUS                        status = STATUS_SUCCESS;
    WDFDEVICE                       device;
    WDF_PNPPOWER_EVENT_CALLBACKS    pnpPowerCallbacks;

    WDF_OBJECT_ATTRIBUTES           attributes;
    t_lpcie_dev_context             *deviceExt;
    WDF_FILEOBJECT_CONFIG           fileConfig;


    UNREFERENCED_PARAMETER(Driver);

    PAGED_CODE();

    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,  "Found new device");

    /* метод доступа к буфером для запросов чтения/записи
      (для IOCTL определяется по коду запроса) */
    WdfDeviceInitSetIoType(DeviceInit, WdfDeviceIoDirect);



    WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);
    pnpPowerCallbacks.EvtDevicePrepareHardware = LpcieEvtPrepareHardware;
    pnpPowerCallbacks.EvtDeviceReleaseHardware = LpcieEvtReleaseHardware;
    pnpPowerCallbacks.EvtDeviceD0Exit = LpcieEvtD0Exit;

    WdfDeviceInitSetPnpPowerEventCallbacks(DeviceInit, &pnpPowerCallbacks);


    /* устанавливаем callback'и на открытие и закрытие файлов */
    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attributes, LPCIE_FILE_CONTEXT);
    attributes.SynchronizationScope = WdfSynchronizationScopeNone;
    attributes.ExecutionLevel = WdfExecutionLevelPassive;
    WDF_FILEOBJECT_CONFIG_INIT(&fileConfig,
                               LpcieEvtDeviceFileCreate,
                               LpcieEvtFileClose,
                               WDF_NO_EVENT_CALLBACK // No cleanup callback function
                               );

    WdfDeviceInitSetFileObjectConfig(DeviceInit, &fileConfig, &attributes);



    /* устанавливаем аттрибуты для запросов к устройству */
    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attributes, t_lpcie_req_context);
    WdfDeviceInitSetRequestAttributes(DeviceInit, &attributes);


    /* для задания контекста устройства устанавливаем атрибуты */
    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attributes, t_lpcie_dev_context);
    //
    // By opting for SynchronizationScopeDevice, we tell the framework to
    // synchronize callbacks events of all the objects directly associated
    // with the device. In this driver, we will associate queues and
    // and DpcForIsr. By doing that we don't have to worrry about synchronizing
    // access to device-context by Io Events and DpcForIsr because they would
    // not concurrently ever. Framework will serialize them by using an
    // internal device-lock.
    //
    //attributes.SynchronizationScope = WdfSynchronizationScopeDevice;

    /* создаем устройство */
    status = WdfDeviceCreate( &DeviceInit, &attributes, &device );

    if (!NT_SUCCESS(status)) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                    "DeviceCreate failed %!STATUS!", status);
    } else {
        deviceExt = LpcieGetDeviceContext(device);
        deviceExt->device = device;
    }

    /* создаем очередь для управляющих запросов */
    if (NT_SUCCESS(status)) {
        WDF_IO_QUEUE_CONFIG queueConfig;

        WDF_IO_QUEUE_CONFIG_INIT( &queueConfig,
                                  WdfIoQueueDispatchSequential);

        queueConfig.EvtIoDeviceControl = LpcieEvtIoControl;

        status = WdfIoQueueCreate( deviceExt->device,
                                   &queueConfig,
                                   WDF_NO_OBJECT_ATTRIBUTES,
                                   &deviceExt->ctlQueue );

        if(!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "WdfIoQueueCreate failed: %!STATUS!", status);
        } else {
            status = WdfDeviceConfigureRequestDispatching(deviceExt->device,
                                                          deviceExt->ctlQueue,
                                                          WdfRequestTypeDeviceControl);

            if(!NT_SUCCESS(status)) {
                TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                            "WdfDeviceConfigureRequestDispatching failed: %!STATUS!",
                            status);
            }
        }
    }

    /* создаем очередь для запросов чтения/записи */
    if (NT_SUCCESS(status)) {
        WDF_IO_QUEUE_CONFIG queueConfig;

        WDF_IO_QUEUE_CONFIG_INIT(&queueConfig, WdfIoQueueDispatchParallel);

        queueConfig.EvtIoRead = LpcieEvtIoRead;
        queueConfig.EvtIoWrite = LpcieEvtIoWrite;

        status = WdfIoQueueCreate( deviceExt->device,
                                   &queueConfig,
                                   WDF_NO_OBJECT_ATTRIBUTES,
                                   &deviceExt->rwQueue );

        if(!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "WdfIoQueueCreate failed: %!STATUS!", status);
        } else {
            status = WdfDeviceConfigureRequestDispatching(deviceExt->device,
                                                          deviceExt->rwQueue,
                                                          WdfRequestTypeRead);

            if(!NT_SUCCESS(status)) {
                TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                            "WdfDeviceConfigureRequestDispatching failed: %!STATUS!",
                            status);
            }
        }

        if (NT_SUCCESS(status)) {
            status = WdfDeviceConfigureRequestDispatching(deviceExt->device,
                                                          deviceExt->rwQueue,
                                                          WdfRequestTypeWrite);
        }

        if(!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "WdfDeviceConfigureRequestDispatching failed: %!STATUS!",
                        status);
        }
    }

    /* создаем очередь для запросов чтения из устройства */
    if (NT_SUCCESS(status)) {
        WDF_IO_QUEUE_CONFIG queueConfig;

        WDF_IO_QUEUE_CONFIG_INIT(&queueConfig, WdfIoQueueDispatchSequential);

        queueConfig.EvtIoRead = LpcieEvtDeviceRead;

        status = WdfIoQueueCreate( deviceExt->device,
                                   &queueConfig,
                                   WDF_NO_OBJECT_ATTRIBUTES,
                                   &deviceExt->readDevQueue );

        if(!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "WdfIoQueueCreate failed: %!STATUS!", status);
        }
    }

    /* создаем очередь для запросов записи в устройство */
    if (NT_SUCCESS(status)) {
        WDF_IO_QUEUE_CONFIG queueConfig;

        WDF_IO_QUEUE_CONFIG_INIT(&queueConfig, WdfIoQueueDispatchSequential);

        queueConfig.EvtIoWrite = LpcieEvtDeviceWrite;

        status = WdfIoQueueCreate( deviceExt->device,
                                   &queueConfig,
                                   WDF_NO_OBJECT_ATTRIBUTES,
                                   &deviceExt->writeDevQueue );

        if(!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "WdfIoQueueCreate failed: %!STATUS!", status);
        }
    }

    /* Создаем объект прерывания для обработки прерываний от устройства */
    if (NT_SUCCESS(status)) {
        WDF_INTERRUPT_CONFIG config;
        WDF_INTERRUPT_CONFIG_INIT(&config, LpcieEvtInterruptIsr,
                                   LpcieEvtInterruptDpc);
        config.EvtInterruptEnable = LpcieEvtInterruptEnable;
        config.EvtInterruptDisable = LpcieEvtInterruptDisable;
        status = WdfInterruptCreate(deviceExt->device,
                                &config,
                                WDF_NO_OBJECT_ATTRIBUTES,
                                &deviceExt->interrupt);

        if(!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "WdfInterruptCreate failed: %!STATUS!",
                        status);
        }
    }


    /* Создаем интерфейс, чтобы к устройству можно было обратиться
     * со стороны пользовательской программы */
    if (NT_SUCCESS(status)) {
        status = WdfDeviceCreateDeviceInterface( device,
                                                &GUID_LPCIE_INTERFACE,
                                                NULL );
        if (!NT_SUCCESS(status)) {
            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "DeviceCreateDeviceInterface failed %!STATUS!", status);
        } else {
            TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,
                        "Successfully create device interface");
        }
    }

    /* читаем VID/PID из конфигурационного пространства PCI, чтобы точно
       знать с каким устройством работаем */
    if (NT_SUCCESS(status)) {
        uint32_t id;
        status = LpcieReadConfigSpace(WdfDeviceWdmGetDeviceObject(deviceExt->device),
                             &id, 0, sizeof(id));
        if (!NT_SUCCESS (status)) {

            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                        "Read config error: %!STATUS!", status);
        } else {
            deviceExt->vid = id&0xFFFF;
            deviceExt->pid = (id>>16)&0xFFFF;
            TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,
                        "new device ids: VID=0x%x, PID=0x%x",
                        deviceExt->vid, deviceExt->pid);
        }
    }

    return status;
}

NTSTATUS LpcieEvtPrepareHardware(
        WDFDEVICE       Device,
        WDFCMRESLIST   Resources,
        WDFCMRESLIST   ResourcesTranslated
    ) {
    NTSTATUS status = STATUS_SUCCESS;
    ULONG res_cnt, i;
    PCM_PARTIAL_RESOURCE_DESCRIPTOR  desc;
    t_lpcie_dev_context *devExt = LpcieGetDeviceContext(Device);

    UNREFERENCED_PARAMETER(Resources);
    PAGED_CODE();

    res_cnt = WdfCmResourceListGetCount(ResourcesTranslated);
    for (i=0; (i < res_cnt) && NT_SUCCESS(status); i++) {
        desc = WdfCmResourceListGetDescriptor(ResourcesTranslated, i);
        if (desc!=NULL) {
            switch (desc->Type) {
                case CmResourceTypeMemory:
                    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,
                                "New resource: memory - addr = %I64X, len = %X",
                                desc->u.Memory.Start.QuadPart, desc->u.Memory.Length);

                    /* первый ресурс памяти - память FPGA - мапим его в
                       адресное пространство ядра */
                    if (!devExt->fpga_regs.regs) {
                        devExt->fpga_regs.regs = MmMapIoSpace(desc->u.Memory.Start,
                                                        desc->u.Memory.Length,
                                                        MmNonCached);
                        devExt->fpga_regs.len = desc->u.Memory.Length/4;
                        if (!devExt->fpga_regs.regs) {
                            TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                                        "Unable to map Registers memory %08I64X, length %d",
                                        desc->u.Memory.Start.QuadPart, desc->u.Memory.Length);
                            status = STATUS_INSUFFICIENT_RESOURCES;
                        }
                    }
                    break;
                case CmResourceTypeMemoryLarge:
                    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,
                                "New resource: Large memory - addr = %I64X, len = %X",
                                desc->u.Memory64.Start.QuadPart, desc->u.Memory64.Length64);
                    break;
                case CmResourceTypeInterrupt:
                    if (desc->Flags &  CM_RESOURCE_INTERRUPT_MESSAGE) {
                        TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,
                                    "New resource: MSI");
                    } else {
                        TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,
                                    "New resource: MSI");
                    }
                    break;
                case CmResourceTypeDma:
                    TraceEvents(TRACE_LEVEL_INFORMATION, DBG_PNP,
                                "New resource: DMA");
                    break;
            }
        }
    }

    if (NT_SUCCESS(status) && !devExt->fpga_regs.regs) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                    "Missing memory resource!");
        status = STATUS_DEVICE_CONFIGURATION_ERROR;
    } else {
        lpcie_read_descr(devExt);
    }


    if (NT_SUCCESS(status)) {
        /* устанавливаем выравнивание на слово для DMA */
        ULONG alignReq;
        WDF_DMA_ENABLER_CONFIG config;
        uint32_t t;

        alignReq = WdfDeviceGetAlignmentRequirement(Device);
        if (alignReq < LPCIE_DMA_ALIGNMENT) {
            WdfDeviceSetAlignmentRequirement(Device, LPCIE_DMA_ALIGNMENT);
        }

        devExt->in_transf_len = LPCIE_DMA_TRANSFER_LEN;
        devExt->out_transf_len = LPCIE_DMA_TRANSFER_LEN;

        for (t=0; (t < LPCIE_MAX_TRANSF_CNT) && (IS_SUCCESS(status)); t++) {

            /* создаем DmaEnabler для разрешения DMA-операций */
            WDF_DMA_ENABLER_CONFIG_INIT(&config, WdfDmaProfileScatterGather64Duplex,
                                        LPCIE_DMA_TRANSFER_LEN);

            status = WdfDmaEnablerCreate(Device, &config, WDF_NO_OBJECT_ATTRIBUTES,
                                         &devExt->dmaEnabler[t]);
            if (!NT_SUCCESS (status)) {
                TraceEvents(TRACE_LEVEL_ERROR, DBG_PNP,
                            "WdfDmaEnablerCreate failed: %!STATUS!", status);
            } else {
                size_t in_len, out_len;
                WdfDmaEnablerSetMaximumScatterGatherElements(devExt->dmaEnabler[t],
                                                             LPCIE_DMA_MAX_PAGES_CNT/LPCIE_MAX_TRANSF_CNT);

                in_len = WdfDmaEnablerGetFragmentLength(devExt->dmaEnabler[t], WdfDmaDirectionReadFromDevice);
                out_len = WdfDmaEnablerGetFragmentLength(devExt->dmaEnabler[t], WdfDmaDirectionWriteToDevice);
                if (in_len < devExt->in_transf_len)
                    devExt->in_transf_len = in_len;
                if (out_len < devExt->out_transf_len)
                    devExt->out_transf_len = out_len;
                TraceEvents(TRACE_LEVEL_VERBOSE, DBG_PNP,
                            "maximum transfer length: in = %d, out = %d", devExt->in_transf_len, devExt->out_transf_len);
            }
        }
    }



    if (NT_SUCCESS(status)) {
        /* убираем сигнал сброса для BlackFin, чтобы можно было
           подключится к нему по JTAG при желании */
        uint32_t reg;
        status = lpcie_fpga_reg_read(devExt, L502_REGS_BF_CTL, &reg);
        reg |= L502_REGBIT_BF_CTL_BF_RESET_Msk;
        if (NT_SUCCESS(status))
            status = lpcie_fpga_reg_write(devExt, L502_REGS_BF_CTL, reg);

    }

    if (NT_SUCCESS(status)) {
        status = lpcie_streams_init(devExt);
    }

    return status;
}


NTSTATUS LpcieEvtReleaseHardware(IN  WDFDEVICE Device,
                                 IN  WDFCMRESLIST ResourcesTranslated) {
    NTSTATUS status = STATUS_SUCCESS;
    t_lpcie_dev_context *devExt = LpcieGetDeviceContext(Device);

    PAGED_CODE();
    if (devExt->fpga_regs.regs) {
        MmUnmapIoSpace(devExt->fpga_regs.regs, devExt->fpga_regs.len*4);
    }
    return status;
}

NTSTATUS LpcieEvtD0Exit (IN WDFDEVICE  Device,
                         IN WDF_POWER_DEVICE_STATE  TargetState) {
    t_lpcie_dev_context *devExt = LpcieGetDeviceContext(Device);
    lpcie_streams_dev_release(devExt);
    return STATUS_SUCCESS;
}



/* В DPC обрабатываем флаги прерываний, которые сохранил в контексте устройства
   обработчик прерывания */
VOID LpcieEvtInterruptDpc (IN WDFINTERRUPT  Interrupt, IN WDFOBJECT  AssociatedObject) {
    t_lpcie_dev_context* dev = LpcieGetDeviceContext(WdfInterruptGetDevice(Interrupt));
    NTSTATUS              status;

    uint32_t irq_state=0;
    /* сохраняем данные о произошедших прерываниях, чтобы их обработать */
    WdfInterruptAcquireLock(Interrupt);
    irq_state = dev->irq_state;
    dev->irq_state = 0;
    WdfInterruptReleaseLock(Interrupt);

    TraceEvents(TRACE_LEVEL_VERBOSE, DBG_DPC, "dpc occured: 0x%x", irq_state);

    lpcie_streams_proc_irq(dev, irq_state);
}





/* обработчик прерывания. В нем мы считываем из устройтва флаги, определяющие,
   какие прерывания произошли и сбрасываем их в устройтве.
   После этого планируем DPC с этими флагами, в которой и будем делать всю
   обработку */
BOOLEAN LpcieEvtInterruptIsr(IN WDFINTERRUPT  Interrupt, IN ULONG  MessageID) {
    BOOLEAN serviced = FALSE;
    uint32_t irq_state, irq_en;
    NTSTATUS status = STATUS_SUCCESS;
    t_lpcie_dev_context* dev = LpcieGetDeviceContext(WdfInterruptGetDevice(Interrupt));


    UNREFERENCED_PARAMETER( MessageID );    

    /* запрещаем все прерывания, чтобы не потерять MSI прерывание, если
     * прерывания произойдут подряд, и второе произойдет после чтения
     * статуса, но до сброса прерываний
     */
    lpcie_fpga_reg_read(dev, L502_REGS_DMA_IRQ_EN, &irq_en);
    lpcie_fpga_reg_write(dev, L502_REGS_DMA_IRQ_DIS, irq_en);


    /* читаем регистр, указывающий, какие прерывания произошли */
    status = lpcie_fpga_reg_read(dev, L502_REGS_DMA_IRQ, &irq_state);
    if (IS_SUCCESS(status) && irq_state) {
        TraceEvents(TRACE_LEVEL_VERBOSE, DBG_INTERRUPT, "isr occured: 0x%x", irq_state);
        /* если были прерывания - то сбрасываем их, и устанавливаем признак,
          что прерывание было наше */
        serviced = TRUE;
        status = lpcie_fpga_reg_write(dev, L502_REGS_DMA_IRQ, irq_state);
        if (IS_SUCCESS(status)) {
            dev->irq_state |= irq_state;
            WdfInterruptQueueDpcForIsr( Interrupt );
        }
    }
    lpcie_fpga_reg_write(dev, L502_REGS_DMA_IRQ_EN, irq_en);
    return serviced;
}


NTSTATUS LpcieEvtInterruptEnable (IN WDFINTERRUPT  Interrupt,
                                  IN WDFDEVICE  AssociatedDevice) {
    TraceEvents(TRACE_LEVEL_VERBOSE, DBG_INTERRUPT, "interrupt enabled");
    return STATUS_SUCCESS;
}

NTSTATUS LpcieEvtInterruptDisable (IN WDFINTERRUPT  Interrupt,
                                  IN WDFDEVICE  AssociatedDevice) {

    TraceEvents(TRACE_LEVEL_VERBOSE, DBG_INTERRUPT, "interrupt disabled");
    return STATUS_SUCCESS;
}


static NTSTATUS LpcieReadConfigSpace(
    IN PDEVICE_OBJECT DeviceObject,
    IN PVOID          Buffer,
    IN ULONG          Offset,
    IN ULONG          Length
    ) {
    KEVENT event;
    NTSTATUS status;
    PIRP irp;
    IO_STATUS_BLOCK ioStatusBlock;
    PIO_STACK_LOCATION irpStack;
    PDEVICE_OBJECT targetObject;

    PAGED_CODE();

    KeInitializeEvent( &event, NotificationEvent, FALSE );

    targetObject = IoGetAttachedDeviceReference( DeviceObject );

    irp = IoBuildSynchronousFsdRequest( IRP_MJ_PNP,
                                        targetObject,
                                        NULL,
                                        0,
                                        NULL,
                                        &event,
                                        &ioStatusBlock );

    if (irp == NULL) {
        status = STATUS_INSUFFICIENT_RESOURCES;
        goto End;
    }

    irpStack = IoGetNextIrpStackLocation( irp );

    irpStack->MinorFunction = IRP_MN_READ_CONFIG;
    irpStack->Parameters.ReadWriteConfig.WhichSpace = PCI_WHICHSPACE_CONFIG;
    irpStack->Parameters.ReadWriteConfig.Buffer = Buffer;
    irpStack->Parameters.ReadWriteConfig.Offset = Offset;
    irpStack->Parameters.ReadWriteConfig.Length = Length;

    //
    // Initialize the status to error in case the bus driver does not
    // set it correctly.
    //

    irp->IoStatus.Status = STATUS_NOT_SUPPORTED ;

    status = IoCallDriver( targetObject, irp );

    if (status == STATUS_PENDING) {
        KeWaitForSingleObject( &event, Executive, KernelMode, FALSE, NULL );
        status = ioStatusBlock.Status;
    }

End:
    //
    // Done with reference
    //
    ObDereferenceObject( targetObject );

    return status;
}



t_status lpcie_fpga_reg_write(t_lpcie_dev_context *devExt, uint32_t addr, uint32_t val) {
    NTSTATUS status = STATUS_SUCCESS;
    if (!devExt->fpga_regs.regs) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_IOCTLS,
                    "FPGA reg write error: memory resource is not allocated!");
        status = STATUS_UNSUCCESSFUL;
    } else if (devExt->fpga_regs.len <= addr) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_FPGA_REGS,
                    "FPGA reg write error: address out of space! addr = 0x%X. reg cnt = 0x%X",
                    addr, devExt->fpga_regs.len);
        status = STATUS_UNSUCCESSFUL;
    } else {
        devExt->fpga_regs.regs[addr] = val;
        TraceEvents(TRACE_LEVEL_VERBOSE, DBG_FPGA_REGS,
                    "write FPGA reg  = 0x%X, val = 0x%X", addr, val);
    }
    return status;
}



t_status lpcie_fpga_reg_read(t_lpcie_dev_context *devExt, uint32_t addr, uint32_t *val) {
    NTSTATUS status = STATUS_SUCCESS;
    if (!devExt->fpga_regs.regs) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_FPGA_REGS,
                    "FPGA reg read error: memory resource is not allocated!");
        status = STATUS_UNSUCCESSFUL;
    } else if (devExt->fpga_regs.len <= addr) {
        TraceEvents(TRACE_LEVEL_ERROR, DBG_FPGA_REGS,
                    "FPGA reg read error: address out of space! addr = 0x%X. reg cnt = 0x%X",
                    addr, devExt->fpga_regs.len);
        status = STATUS_UNSUCCESSFUL;
    } else {
        *val = devExt->fpga_regs.regs[addr];
        TraceEvents(TRACE_LEVEL_VERBOSE, DBG_FPGA_REGS,
                    "read FPGA reg  = 0x%X, val = 0x%X", addr, *val);
    }
    return status;
}
