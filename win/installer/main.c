#include <windows.h>
#include <tchar.h>
#include <stdlib.h>
#include <stdio.h>
#include <difxapi.h>
#include <cfgmgr32.h>

static VOID __cdecl
LogCallback(
    DIFXAPI_LOG Event,
    DWORD Error,
    const TCHAR * pEventDescription,
    PVOID CallbackContext
    );

static void f_save_reboot_flag(int flag)
{
    FILE* f = fopen("reboot", "w");
    if (f)
    {
        fprintf(f, "%d", flag);
        fclose(f);
    }
}


int __cdecl _tmain(int argc, TCHAR** argv)
{
    int err = 0;

    if (argc < 3)
    {
        _tprintf(TEXT("Insufficient arguments!\n"));
        return -1;
    }
    else
    {
        BOOL needReboot=FALSE;
        TCHAR cmd = argv[1][0];
        TCHAR* par = argv[2];
        DIFXAPISetLogCallback( LogCallback, (PVOID)NULL );

        if (cmd==TEXT('u'))
        {
            err = DriverPackageUninstall(par, DRIVER_PACKAGE_DELETE_FILES | DRIVER_PACKAGE_FORCE
                                         | DRIVER_PACKAGE_SILENT,
                                         NULL, &needReboot);
            if (err)
            {
                _tprintf(TEXT("Uninstall error 0x%x\n"), err);
            }
            else
            {
                f_save_reboot_flag(needReboot);
            }
        }
        else if (cmd==TEXT('i'))
        {
            TCHAR* inf_file = par;            
            DEVINST root;
            CONFIGRET cfg_err = CM_Locate_DevNode(&root, NULL, CM_LOCATE_DEVNODE_NORMAL);
            if (cfg_err==CR_SUCCESS)
            {
                cfg_err = CM_Reenumerate_DevNode(root,
                                                 CM_REENUMERATE_SYNCHRONOUS);
                if (cfg_err!=CR_SUCCESS)
                {
                    _tprintf(TEXT("CM_Reenumerate_DevNode error 0x%x\n"), cfg_err);
                }

            }
            else
            {
                 _tprintf(TEXT("CM_Locate_DevNode error 0x%x\n"), cfg_err);
            }





            err = DriverPackageInstall (inf_file, DRIVER_PACKAGE_LEGACY_MODE
                                        | DRIVER_PACKAGE_FORCE
                                        //| DRIVER_PACKAGE_SILENT
                                        , NULL, &needReboot);
            if (err==ERROR_NO_SUCH_DEVINST)
            {
                err = 0;
            }

            if (err)
            {
                _tprintf(TEXT("Install error 0x%x\n"), err);
            }
            else
            {
                DWORD NumOfChars = 0;
                PTCHAR DriverStoreInfPath = 0;

                _tprintf(TEXT("Install driver successfully!\n"));

                // Call DriverPackageGetPath to retrieve the
                // length, in characters, of the requested path of the
                // DIFx driver store INF file
                // The function returns the required length in NumOfChars
                DriverPackageGetPath( inf_file, NULL, &NumOfChars );


                // Allocate an output buffer to retrieve the DIFx driver store INF file path
                DriverStoreInfPath = (PTCHAR) LocalAlloc(LPTR, NumOfChars * sizeof(TCHAR));

                // Retrieve the DIFx driver store INF path
                err = DriverPackageGetPath( inf_file, DriverStoreInfPath, &NumOfChars);
                if (err)
                {
                    _tprintf(TEXT("Get package path error 0x%x\n"), err);
                }
                else
                {
                    _tprintf(TEXT("install path = %s\n"), DriverStoreInfPath);
                    if (argc > 3)
                    {
                        FILE* f = fopen(argv[3], "w");
                        if (f)
                        {
                            fprintf(f, "%s", DriverStoreInfPath);
                            fclose(f);
                        }
                    }
                    f_save_reboot_flag(needReboot);
                }
            }
        }
        else
        {
            err = -3;
        }

        DIFXAPISetLogCallback( NULL, (PVOID)NULL );

        if (needReboot)
        {
            _tprintf(TEXT("Need reboot!\n"));
        }
    }

    return err;
}


VOID __cdecl
LogCallback(
    DIFXAPI_LOG Event,
    DWORD Error,
    const TCHAR * pEventDescription,
    PVOID CallbackContext
    )
{
    UNREFERENCED_PARAMETER(CallbackContext);
    if (0==Error){
        _tprintf( TEXT("LOG Event: %u, %s\n"), Event, pEventDescription );
    } else {
        _tprintf( TEXT("LOG Event: %u, Error = %u, %s\n"), Event, Error, pEventDescription);
    }
}
