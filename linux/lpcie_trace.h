#ifndef LPCIE_TRACE_H
#define LPCIE_TRACE_H

#include <linux/kernel.h>

#define TRACE_LEVEL_ERROR           1
#define TRACE_LEVEL_WARNING         2
#define TRACE_LEVEL_INFORMATION     3
#define TRACE_LEVEL_VERBOSE         4
#define TRACE_LEVEL_DEBUG           5



#define DBG_INIT                    1
#define DBG_DMA                     2


#define LPCIE_TRACE_LEVEL TRACE_LEVEL_INFORMATION
//#define LPCIE_TRACE_LEVEL TRACE_LEVEL_DEBUG


static const char *f_err_msg[] = {
    KERN_CRIT,
    KERN_ERR,
    KERN_WARNING,
    //KERN_NOTICE,
    KERN_INFO,
    KERN_INFO,
    KERN_DEBUG,
    KERN_DEBUG,
    KERN_DEBUG
};

#define TraceEvents(lvl, flag, fmt, args...) do { \
        if (lvl <= LPCIE_TRACE_LEVEL) { \
            printk("%s" "lpcie: " fmt, f_err_msg[lvl], ## args); \
        } \
    } while(0);



#if  LPCIE_TRACE_LEVEL == TRACE_LEVEL_DEBUG
    #define PDEBUG(fmt, args...) TraceEvents(TRACE_LEVEL_DEBUG, 0, fmt, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif

    #define PERROR(fmt, args...) TraceEvents(TRACE_LEVEL_ERROR, 0, fmt, ## args)



#endif // TRACE_H
