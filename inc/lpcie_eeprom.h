#ifndef L5XX_EEPROM_H__
#define L5XX_EEPROM_H__

#include "lpcie_drv.h"

#define L5XX_DEVNAME_SIZE  32
#define L5XX_SERIAL_SIZE   32

#define L502_EEPROM_ADDR_DESCR  0x1F0000UL
#define L502_DESCR_MAX_SIZE     0x010000UL


#define L5XX_EEPROM_FORMAT      1
#define L5XX_DESCR_MIN_SIZE   (sizeof(t_l5xx_eeprom_hdr)+4)
#define L5XX_EEPROM_SIGN      0x4C524F4D

typedef struct
{
    uint32_t sign;
    uint32_t size;
    uint32_t format;
    char name[L5XX_DEVNAME_SIZE];
    char serial[L5XX_SERIAL_SIZE];
    char res[64-12];
} t_l5xx_eeprom_hdr;


#endif
