/*================================================================================================*
 * Конфигурация библиотеки FAST_CRC
 *================================================================================================*/

#ifndef FAST_CRC_CFG_H_
#define FAST_CRC_CFG_H_

#ifdef _WIN32
#include <ntddk.h>

#define FASTCRC_U8_TYPE             UCHAR
#define FASTCRC_U16_TYPE            USHORT
#define FASTCRC_U32_TYPE            ULONG
#define FASTCRC_SIZE_TYPE           size_t
#else
#include <linux/types.h>
#define FASTCRC_U8_TYPE             __u8
#define FASTCRC_U16_TYPE            __u16
#define FASTCRC_U32_TYPE            __u32
#define FASTCRC_SIZE_TYPE           size_t
#endif

/* Начальные значения CRC */
#define CRC16_START_VAL             0
#define CRC32_START_VAL             0
/*================================================================================================*/

/*================================================================================================*/
/* Разрешение компиляции отдельных функций */
#define FASTCRC_CRC16_ADD8          1   /* добавление в CRC16 байта */
#define FASTCRC_CRC16_ADD16         1   /* добавление в CRC16 16-битного слова */
#define FASTCRC_CRC16_BLOCK8        1   /* Вычисление CRC16 блока байтов */
#define FASTCRC_CRC16_BLOCK16       1   /* Вычисление CRC16 блока 16-битных слов */
#define FASTCRC_CRC32_BLOCK8        1   /* Вычисление CRC32 блока байтов */
/*================================================================================================*/

#endif /* FAST_CRC_CFG_H_ */
