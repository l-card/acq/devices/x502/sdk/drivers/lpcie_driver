#ifndef LPCIE_STREAMS_H_
#define LPCIE_STREAMS_H_

#include "lpcie_drv.h"
#include "lpcie_ioctls.h"

t_status lpcie_streams_init(t_lpcie_dev_context* dev);
void lpcie_streams_dev_release(t_lpcie_dev_context* dev);
t_status lpcie_stream_start(t_lpcie_dev_context* dev, uint32_t ch, int single);
t_status lpcie_stream_stop(t_lpcie_dev_context *dev, uint32_t ch);
t_status lpcie_stream_free(t_lpcie_dev_context *dev, uint32_t ch);

t_status lpcie_stream_get_rdy_size(t_lpcie_dev_context *dev, uint32_t ch, uint32_t *rdy_size);

t_status lpcie_stream_cycle_load(t_lpcie_dev_context *dev, const t_lpcie_cycle_set_par* params);
t_status lpcie_stream_cycle_switch(t_lpcie_dev_context *dev, const t_lpcie_cycle_evt_par *params);
t_status lpcie_stream_cycle_stop(t_lpcie_dev_context *dev, const t_lpcie_cycle_evt_par* params);
t_status lpcie_stream_cycle_setup_done(t_lpcie_dev_context *dev, uint32_t ch, uint32_t *done);

void lpcie_streams_proc_irq(t_lpcie_dev_context *dev, uint32_t irq_state);
void lpcie_streams_proc_transf(t_lpcie_dev_context *dev, uint32_t ch);
t_status lpcie_proc_req(t_lpcie_dev_context *dev, t_lpcie_dma_state *state, t_lpcie_req_context *req);
t_status lpcie_streams_set_params(t_lpcie_dev_context *dev,
                              const t_lpcie_stream_ch_params* params,
                              size_t size);
t_status lpcie_streams_get_params(t_lpcie_dev_context *dev,
                              uint32_t ch,
                              t_lpcie_stream_ch_params* params,
                              size_t* size);


#define LPCIE_STREAM_PC_RDY(state) (state->pcbuf_params->state == LPCIE_DMA_BUF_STATE_INVALID ? 0 : state->pcbuf_params->rdy_size)
#endif
